Template.accountbase.helpers({
    neuesKonto: function() {
        return !this._id;
    },
    localaccount: function() {
        return this.localaccount;   
    }
});

Template.accountbase.events({
    'click #resetpassword': function(e){  
        Meteor.call('resetpassword', currentUserId, function(error, response) {
            if(!error){
                Router.go('administration');
            } else {
                alert(error);   
            }
        }); 
    }
});


var currentUserId;
Template.accountbase.rendered = function () {
    currentUserId = this.data._id;
};

AutoForm.hooks({
  editUseraccount: {
        // Called at the beginning and end of submission, respectively.
        // This is the place to disable/enable buttons or the form,
        // show/hide a "Please wait" message, etc. If these hooks are
        // not defined, then by default the submit button is disabled
        // during submission.
        beginSubmit: function(formId, template) {

            var useraccount;
            var displayname = $('#displayname-input').val();
            var email = $('#email-input').val();
            var admin = $('#admin-input').is(':checked');
            var lockedout = $('#lockedout-input').is(':checked');
            
            $("#updateUseraccountError").hide(); 
            
            if(currentUserId) {
                useraccount = {
                    displayname: displayname,
                    email: email,
                    admin: admin,
                    lockedout: lockedout
                };
            } else {
                var username = $('#username-input').val();
                useraccount = {
                    username: username,
                    displayname: displayname,
                    email: email,
                    admin: admin,
                    lockedout: lockedout,
                    localaccount: true
                };
            }
           
            Meteor.call('updateUseraccount', currentUserId, useraccount, function(error, response) {
                if(error){
                    
                    var fehlermeldung = error.toString();
                    if(fehlermeldung.indexOf("Error: ") > -1)
                        fehlermeldung = fehlermeldung.substring(7);
                   
                    if(fehlermeldung.indexOf("[400]") > -1)
                        fehlermeldung = fehlermeldung.substring(0, fehlermeldung.indexOf("[400]") );
                    
                    $("#updateUseraccountError").show(); 
                    $("#updateUseraccountError").text(fehlermeldung);
                } else {
                    Router.go('administration');
                }
            }); 
        }
    }
});