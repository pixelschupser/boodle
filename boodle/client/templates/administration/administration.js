/** This file is part of Boodle.
  *
  * Boodle is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * Boodle is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with Boodle.  If not, see <http://www.gnu.org/licenses/>.
  */
// sets the properties for a reactive table
function createTable(collection) {
    return {
        collection: collection,
        showNavigation: 'never',
        rowsPerPage: 1000000,
        showFilter: true,
        fields: [
            { key: 'username', label: 'Benutzername' },
            { key: 'displayname', label: 'Anzeigename' },
            { key: 'email', label: 'E-Mail' },
            { key: 'localaccount', label: 'Konto', fn: function(value, object) { return value ? "Lokal" : "LDAP"; } },
            { key: 'admin', label: 'Admin', fn: function(value, object) { return value ? "ja" : "nein"; } },
            { key: 'lockedout', label: 'Status', fn: function(value, object) { return value ? "gesperrt" : "aktiv"; } }
        ]
    };
};

Template.administration.rendered = function() {  
  //set Navi-Element to active
  $('.nav-item').removeClass('active');
  $('.nav-item .administration').parent('.nav-item').addClass('active');
  //load reactive/datatables
  if($('.table-striped.reactive-table').height() >= 300){
    $('.table-striped.reactive-table').DataTable({
        info: false,
        ordering: true,
        paging: false,
        searching: false,
        scrollX: true,
        scrollY: 350
    });
  }
  else {
      $('.table-striped.reactive-table').DataTable({
          info: false,
          ordering: true,
          paging: false,
          searching: false,
          scrollX: true
      });
  }
  $('.dataTables_scrollHeadInner').width('100%');
  $('.reactive-table.dataTable').width('100%');
}

Template.administration.helpers({
    // returns the properties for a reactive table 
    // for all users in db
    accounts: function() {
        var collection = Members.find();
        var result = createTable(collection);
        return result;
    }
});

// event handling for eventList
Template.administration.events({
    'click .reactive-table tr': function() {
        Router.go("account", {_id: this._id});
    },
    'click .newaccount': function(e) {
        e.preventDefault();
        Router.go("newaccount");
    }
});