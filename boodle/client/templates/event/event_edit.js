/** This file is part of Boodle.
  *
  * Boodle is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * Boodle is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with Boodle.  If not, see <http://www.gnu.org/licenses/>.
  */
// subscribes to server changes to the events collection
Meteor.subscribe('events');

Template.eventEdit.events({
    'click #delSuggestions': function(e){  
        deleteCalendarSuggestion(currentSuggestion);
    },
    'click #saveSuggestions': function(e){
        saveCalendarSuggestion(currentSuggestion);
    },
    'click .btn-back': function(e) {
        e.preventDefault();
        Router.go("eventPage", {_id: this._id});
    }
});

//The Current Suggestion from the Calendar
var currentSuggestion;

// helper functions for eventEdit
Template.eventEdit.helpers({
    // return te radiobuttons html according to the selected response mode
    // FIXME: use rendered function to set radiobutton values
    getResponseMode: function() 
    {
        var multiButton = '<input type="radio" name="response-mode" id="response-mode-multi" ';
        var singleButton = '<input type="radio" name="response-mode" id="response-mode-single" ';
        var responseModes = {
            multi: this.responseMode == "multi" ? multiButton + 'checked="true"/>' : multiButton + '/>',
            single: this.responseMode == "single" ? singleButton + 'checked="true"/>' : singleButton + '/>'
        } 
        return responseModes;
    },
    curSuggestion: function()
    {
        return currentSuggestion;
    }
});

var currentEventId;
Template.eventEdit.rendered = function () {
    //set Navi-Element to active
    $('.nav-item').removeClass('active');
    currentEventId = this.data._id;
    
    //Init Taginput with Autocomplete
    participantsTagInput(false);
    
    // check if this is a hidden poll and set the checkbox value accordingly
    if (this.data.hidden) 
    {
        $('#hidden-poll-checkbox').prop('checked', true);
    }
    // check if this is a hidden poll and set the checkbox value accordingly
    if (this.data.hidden) 
    {
        $('#hidden-poll-checkbox').prop('checked', true);
    }
    
    //Collect Current Events and add to Calendar
    fillCalendarWithSuggestions(currentEventId, calendarClickEvent);
};

var calendarClickEvent = function(calEvent) { currentSuggestion = calEvent; }

AutoForm.hooks({
  editEvent: {
        // Called at the beginning and end of submission, respectively.
        // This is the place to disable/enable buttons or the form,
        // show/hide a "Please wait" message, etc. If these hooks are
        // not defined, then by default the submit button is disabled
        // during submission.
        beginSubmit: function(formId, template) {
            
            $("#eventError").hide();
            // create the event from the form 
            var event = createEventObject(false);
            
            if(event.suggestions == null ||event.suggestions == undefined || event.suggestions.length == 0) {
                 $("#eventError").text("Ein Event kann nur mit Terminvorschlägen gespeichert werden.");
                 $("#eventError").show();
            } else {
            
                Meteor.call('updateEvent', currentEventId, event, function(error, response) {
                    if(!error){
                       sendEventEmail(currentEventId, notificationStates.EVENT_CHANGES);
                       Router.go('eventPage', {_id: currentEventId});
                    }
                }); 
            }
        }
    }
});
