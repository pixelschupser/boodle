/** This file is part of Boodle.
  *
  * Boodle is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * Boodle is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with Boodle.  If not, see <http://www.gnu.org/licenses/>.
  */
sendEventEmail = function(currentEventId, state) {
    var event = Events.findOne({_id: currentEventId});
    //var link = "Hier geht's zur Abstimmung.".link("https://boodle.beuth-hochschule.de/event/" + currentEventId);
    var link = "https://boodle.beuth-hochschule.de/event/" + currentEventId;
    var creator = event.creator;
    var eventtitle = event.title;
    var description = event.description;
    var kontoCreator = Members.findOne({username: creator});
    
    if(kontoCreator) {
        event.participants.forEach(function(participant) {

            if(participant != creator) {
                var konto = Members.findOne({username: participant});
                if(konto && notificationEnabled(konto, state)) {

                    Meteor.call('sendEmail',
                        konto.email, 
                        kontoCreator.email, 
                        creator + state.title, 
                        eventtitle + "\n\n" + description + "\n\n" + link,
                        function(error, response) {
                            if (error != undefined) {
                                Meteor.log.error("Was not able to send mail to " + participant + state.error + currentEventId);
                            }
                        });
                }
            }
        });  
    }
};

var notificationEnabled = function(user, state) {
    if(user && user.notifications) {
        switch (state.value) {
            case notificationStates.EVENT_INVITATION.value:
                return user.notifications.emailForEventInvitation;
            case notificationStates.EVENT_CHANGES.value:
                return user.notifications.emailForEventChanges;
            case notificationStates.EVENT_CANCEL.value:
                return user.notifications.emailForEventCancel;
            case notificationStates.EVENT_DECIDE.value:
                return user.notifications.emailForEventDecide;
        }
    }
    return true;
}

notificationStates = {
  EVENT_INVITATION : {value: "eventinvitation", title: " hat Sie zu einem Termin eingeladen", error: " for created event with id "}, 
  EVENT_CHANGES: {value: "ceventhanges", title: " hat einen Termin zu dem Sie eingeladen sind bearbeitet", error: " for updated event with id "},
  EVENT_CANCEL : {value: "eventcancel", title: "hat für ein Event zu dem Sie eingeladen sind abgesagt", error: " for canceled event with id "},
  EVENT_DECIDE: { value: "eventdecide", title: " hat für ein Event zu dem Sie eingeladen sind einen Termin ausgewählt", error: " for picked event with id " } 
};