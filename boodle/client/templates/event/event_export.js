/** This file is part of Boodle.
  *
  * Boodle is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * Boodle is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with Boodle.  If not, see <http://www.gnu.org/licenses/>.
  */

/*Exportiert alle events, egal welchen status sie haben.*/
exportEvents_All = function() {
    //init ics export
    var cal = ics();    
    
    //GetEvents from DB where user is Creator or Paticipant
    var collection = Events.find( { 
        $or:[{creator: Meteor.user().username}, 
             {participants: Meteor.user().username}]})
        .forEach(function(event){    
        Meteor.log.debug("Exporting event:"+event.description);
        $.each(event.suggestions, function(n, suggestion){
            //Event zum export hinzufügen
            if(suggestion.state != 'deleted'){
                cal.addEvent(
                    suggestion.title, 
                    suggestion.description, 
                    "", 
                    suggestion.start, 
                    suggestion.end);
            }
            
        });
    });
    return cal;
};

/*Exportiert alle beschlossenen events*/
exportEvents_Fixed = function() {
    //init ics export
    var cal = ics();    
    
    //GetEvents from DB where user is Creator or Paticipant
    var collection = Events.find( { 
        $or:[{creator: Meteor.user().username}, 
             {participants: Meteor.user().username}]})
        .forEach(function(event){    
        Meteor.log.debug("Exporting event:"+event.description);
        $.each(event.suggestions, function(n, suggestion){
            //Event zum export hinzufügen
            if(suggestion.state == 'fix'){
                cal.addEvent(
                    suggestion.title, 
                    suggestion.description, 
                    "", 
                    suggestion.start, 
                    suggestion.end);
            }
            
        });
    });
    return cal;
};

/*Exportiert alle Offenen Events*/
exportEvents_Open = function() {
    //init ics export
    var cal = ics();    
    
    //GetEvents from DB where user is Creator or Paticipant
    var collection = Events.find( { 
        $or:[{creator: Meteor.user().username}, 
             {participants: Meteor.user().username}]})
        .forEach(function(event){    
        Meteor.log.debug("Exporting event:"+event.description);
        $.each(event.suggestions, function(n, suggestion){
            //Event zum export hinzufügen
            if(suggestion.state == 'open'){
            cal.addEvent(
                suggestion.title, 
                suggestion.description, 
                "", 
                suggestion.start, 
                suggestion.end);
            }
        });
    });
    return cal;
};