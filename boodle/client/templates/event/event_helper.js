/** This file is part of Boodle.
  *
  * Boodle is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * Boodle is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with Boodle.  If not, see <http://www.gnu.org/licenses/>.
  */
// TO DO: comment
calendarEventToSuggestion = function(evt) {
    return {
        title: evt.title,
        description: "",
        start: evt.start.toDate(),
        startLocalized: moment(evt.start).format('DD.MM.YYYY hh:mm'),
        end: evt.end.toDate(),
        endLocalized: moment(evt.end).format('DD.MM.YYYY hh:mm'),
        state: 'open'
    };
}

//Ermittelt alle bearbeitbarten Terminvorschlaege im Kalender und gibt diese als Array zurueck
collectCalendarSuggestions = function() {
    //get all new events from calendar
    var clientEvents = $('.calendar').fullCalendar('clientEvents',
            function (event) {
                return event.editable;
            });
    var suggestions = [];
    $.each(clientEvents, function(i, o){
        var suggestion = calendarEventToSuggestion(o);
        suggestion.state = suggestionStates.OPEN.value;
        suggestion.color = suggestionStates.OPEN.color;
        suggestions.push(suggestion);
    });   
    return suggestions;
}

//Speichert den uebergebenen Terminvorschlag aus dem Kalendar
deleteCalendarSuggestion = function(currentSuggestion) {
    $('.calendar').fullCalendar('removeEvents', currentSuggestion._id);
    $('#eventModal').modal('hide');
}

//Speichert den uebergebenen Terminvorschlag im Kalendar
saveCalendarSuggestion = function(currentSuggestion) {
    currentSuggestion.title = $('#suggestions-title').val();
    $('.calendar').fullCalendar('updateEvent', currentSuggestion);
    $('#eventModal').modal('hide');
}

//Erstellt fuer das insert oder update eines Events ein Objekt
createEventObject = function(withCreator) {
    var title = $('#title-input').val();
    var description = $('#description-input').val();
    var participants = $('#participants-input').tokenfield('getTokensList',';').split(';');
    var suggestions = collectCalendarSuggestions();
    var responseMode = $('#responseMode-input').val();
    var hidden = $('#hidden-input').is(':checked');
    var onlyYesNoAnswer = $('#onlyYesNoAnswer-input').is(':checked');
    
    var event;
    
    if(withCreator) {
        console.log("with Creator");
        event = {
                title: title,
                description: description,
                creator: Meteor.user().username,
                participants: participants,
                suggestions: suggestions,
                responseMode: responseMode,
                hidden: hidden,
                onlyYesNoAnswer: onlyYesNoAnswer
        };
    }else{
        console.log("without Creator");
        event = {
            title: title,
            description: description,
            participants: participants,
            suggestions: suggestions,
            responseMode: responseMode,
            hidden: hidden,
            onlyYesNoAnswer: onlyYesNoAnswer
        };
    }
    
    return event;
}

//Initalisiert den Kalender fuer die Darstellung alle Terminvorschlaege
fillCalendarWithSuggestions = function(currentEventId, clickEvent) {
    //Collect Current Events and add to Calendar
    var events = [];
    var collection = Events.find( { 
        $or:[{creator: Meteor.user().username}, 
             {participants: Meteor.user().username}]})
        .forEach(function(event){    
        $.each(event.suggestions, function(n, suggestion){
            if(event.deleteEvent == 'true'){
                return false;
            }
            if(event._id != currentEventId){
                suggestion.editable = false;
            }else{
                suggestion.editable = true;
                suggestion.className = 'editable';
            }
            if(suggestion.state != 'deleted'){
                events.push(suggestion); 
            }
        });
    });
    
    //Init Calendar
    $('.calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month, agendaWeek, agendaDay'
        },
        defaultView: 'month',
        editable: clickEvent != null,
        dayClick: function(date, jsEvent, view) {
            if(clickEvent != null) {
                //If Month view go to AgendaWeek
                if(view.name == "month"){
                    $('.calendar').fullCalendar( 'changeView', "agendaWeek" );
                    $('.calendar').fullCalendar('gotoDate', date);
                }              
                else{
                    if(moment(date).diff(moment()) <= 0){
                        alert('Die anlage von Terminen in der Vergangenheit ist nicht möglich.');
                        return;
                    }
                    //Add appointment to Calendar for the Current Event
                    var event ={
                        title: $('#title-input').val(),
                        start: moment(date),
                        end: moment(date).add(1, 'hour'),
                        editable: true,
                        color: suggestionStates.OPEN.color,
                        className: 'editable'
                    };  
                    $('.calendar').fullCalendar('renderEvent', event );
                }
            }
        },
        eventDrop: function(event, delta, revertFunc) {
            if(moment(event.start).diff(moment()) <= 0){
                alert('Die anlage von Terminen in der Vergangenheit ist nicht möglich.');
                revertFunc();
            }
        },
        eventClick:function(calEvent, jsEvent, view) {
            if(clickEvent != null && calEvent.editable){                
                //Have to go here over a global var, cuz Meteor session destroyes Moment object...
                clickEvent(calEvent);            
                $('#suggestions-title').val(calEvent.title);
                $('#eventModal').modal();
            }
        },
        lang: 'de',
        events: events,
        timeFormat: 'hh:mm'
    });
    $('.calendar .fc-toolbar').after($("ul.legend-list"));
}

// TO DO: comment
getSuggestionIdentifier = function(date) {
    //Split StartEvent
    splitStart =  date == undefined ? ["", ""] : date.startLocalized.split(' ');
    // get Date
    startDate = splitStart[0]; 
    // get Time
    startTime = splitStart[1];
    splitEnd = date == undefined ? ["", ""] :  date.endLocalized.split(' ');
    endDate = splitEnd[0];
    endTime = splitEnd[1];
    if (startDate == endDate){
        return date == undefined ? ""  : date.startLocalized + "_" + endTime;
    }
    else {
        return date == undefined ? "" : date.startLocalized + "_" + date.endLocalized;
    } 
}
// TO DO: comment
getFullSuggestionIdentifier = function(date) {
    return date.startLocalized + "_" + date.endLocalized;
}
// TO DO: comment
participantsSearchEngine = function() {
    Meteor.log.debug("Initialisiere Suchengine für mögliche Teilnehmer...");
    var engine = new Bloodhound({
        local: function() {
            var users = new Array()
            Members.find().forEach(function(user){
                var display = { display:user.displayname+"("+user.username+")", value: user.username };
                users.push(display);
            });
            return users;
        },
        datumTokenizer: function (d) {
            return Bloodhound.tokenizers.whitespace(d.value);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace
    });
    engine.initialize();
    Meteor.log.debug("Initialisiert!");
    return engine;
}

participantsTagInput = function(setTokens) {
    //Init TagInput with autocomplete
    $('#participants-input').tokenfield({
        typeahead: [null, {
            source: participantsSearchEngine().ttAdapter(),
            displayKey: 'display',
            limit: 10
        }]
    });
    
    if(setTokens)
        $('#participants-input').tokenfield('setTokens', Meteor.user().username);
}

suggestionStates = {
  OPEN : {value: "open", color: "rgba(0,238,204, 0.6)", name: "vorgeschlagen"}, // or this color (0,221,238)
  FIXED: {value: "fix", color: "#ff8888", name: "beschlossen"}, //set to rose,
  DELETED : {value: "deleted", color: "#787878", name: "gelöscht"} // set to grey

};
