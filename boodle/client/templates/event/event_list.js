/** This file is part of Boodle.
  *
  * Boodle is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * Boodle is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with Boodle.  If not, see <http://www.gnu.org/licenses/>.
  */
// event handling for eventList
Template.eventList.events({
    'click .reactive-table tr': function() {
        Router.go("eventPage", {_id: this._id});
    },
    'click #exportEvents': function() {
        exportEvents_All().download("Boodle_Events_All");
    }
});
// sets the properties for a reactive table
function createTable(collection) {
    return {
        collection: collection,
        showNavigation: 'never',
        rowsPerPage: 1000000,
        showFilter: true,
        fields: [
            { key: 'creator', label: 'Erstellt von' },
            { key: 'title', label: 'Titel' },
            { key: 'description', label: 'Beschreibung' },
            { key: 'participants', label: 'Teilnehmer' }
        ]
    };
};

Template.eventList.rendered = function() { 
    //set Navi-Element to active
    $('.nav-item').removeClass('active');
    $('.nav-item .event-overview').parent('.nav-item').addClass('active');
    //load reactive/datatables
    if($('.table-striped.reactive-table').height() >= 300){
        $('.table-striped.reactive-table').DataTable({
            info: false,
            ordering: true,
            paging: false,
            searching: false,
            scrollX: true,
            scrollY: 450
        });
    }
    
    else if($('.table-striped.reactive-table').height() <= 300){
        $('.table-striped.reactive-table').DataTable({
            info: false,
            ordering: true,
            paging: false,
            searching: false,
            scrollX: true,
            scrollY: false
        });
    }
    $('.reactive-table.dataTable').width('100%');
    //Collect Current Events and add to Calendar
    fillCalendarWithSuggestions(null, null);
}

// helper functions for eventList
Template.eventList.helpers({
    // returns the properties for a reactive table 
    // for all events this user administrates
    administrated: function() {
        var currentUser = Meteor.user();
        var collection = Events.find({creator: currentUser.username});
        return createTable(collection);
    },
    // returns the properties for a reactive table 
    // for all events this user is invited to
    invited: function() {
        var currentUser = Meteor.user();
        var collection = Events.find({participants: currentUser.username});
        return createTable(collection);
    },
    currentUserAvailable: function() {
        if(Meteor.user())
            return true;
        else
            return false;
    }
});

