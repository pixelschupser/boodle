/** This file is part of Boodle.
  *
  * Boodle is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * Boodle is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with Boodle.  If not, see <http://www.gnu.org/licenses/>.
  */
// subscribes to server changes to the events collection
Meteor.subscribe('events');

Template.eventPage.events({
	'click .event-edit': function(e) {
		e.preventDefault();
	},
	//go to edit-page of this event
	'click .edit-event': function(e) {
        Router.go("eventEdit", {_id: this._id});
	},
	//go to picker-page of this event to set/choose an opption
	'click .pick-event': function(e) {
        Router.go("eventPicker", {_id: this._id});
	},
    //show overlay with notifikation
    'click .close-event': function(e) {
        $('#deleteNote').modal();
        var currentEventId = this._id;

        $('#submit-delete').click(function(){
            $('body').removeClass('modal-open');
            $('#deleteNote').hide();
            $('div.modal-backdrop.fade.in').remove();
        
            var event = Events.findOne({_id: currentEventId});
            event.deleteEvent = 'true';
            $.each(event.suggestions, function(i,o){
                o.state = suggestionStates.DELETED.value;
                o.color = suggestionStates.DELETED.color;
            })
            delete event._id;
            delete event.creator;
            Meteor.call('updateEvent', currentEventId, event, function(error, response) { 
                if (error) {
                    Meteor.log.error(error);
                }
                else {
                    $('.vote-table').addClass('disabled');
                    $('.availability input').attr('disabled', 'true');
                    $('.state-description').show();
                    $('.state-deleted').show();
                    $('.item-list.event.dropdown').hide();
                    sendEventEmail(currentEventId,notificationStates.EVENT_CANCEL);
                }
            });
        });
    }
});

// helper functions for eventEdit
Template.eventPage.helpers({

    // return the response mode as a string according to the selected response mode
    getResponseMode: function() {
        // FIXME: use constants to replace response mode string
        return this.responseMode == "multi" ? "Ja-Nein-Vielleicht" : "Eine Option pro Zeile UND Spalte";
    },
    // return the response mode as a string according to the selected response mode
    isHidden: function() 
    {
        // FIXME: use constants to replace response mode string
        return this.hidden == true ? "Ja" : "Nein";
    },
    // return the state of this event
    getState: function() {
        return this.state === "closed" ?  "Geschlossen" : "Laufend" ;
    },
    // set Class ChossenEvent to Section "eventItem"
    eventChosen: function() {
    	return this.finalSuggestion != null && this.finalSuggestion != undefined && this.finalSuggestion != "" ?  "choosenEvent" : "" ;
    },
    selectedEvent: function() {
        return this.finalSuggestion != null && this.finalSuggestion != undefined && this.finalSuggestion != "" ?  "selected" : "" ;
    },
    // if the Event is deleted add Class deleted to the descriptionelement
    deletedEvent: function() {
        return this.deleteEvent != null && this.deleteEvent != undefined && this.deleteEvent != "" ?  "deleted" : "" ;
    }
});

Template.eventPage.rendered = function() { 
    //set Navi-Element to active
    $('.nav-item').removeClass('active');
}
/*
Template.suggestion.helpers({
	setReadOnly: function() {
    	return this.finalSuggestion != null && this.finalSuggestion != undefined && this.finalSuggestion != "" ?  "readonly" : "" ;
    }
});
*/
