/** This file is part of Boodle.
  *
  * Boodle is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * Boodle is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with Boodle.  If not, see <http://www.gnu.org/licenses/>.
  */
// subscribes to server changes to the events collection
Meteor.subscribe('events');

Template.eventPicker.events({
    'click .select-form .btn-primary': function(e) {
        e.preventDefault();
        var currentEventId = this._id;
        var chooseSuggestion = " ";

        var isChecked = $(".btn-pick-event");        
        // if no input-feld is set/checked display error
        if(!$(isChecked).is(':checked')){
            $("p.error.missingEvent").show();
        }
        // if is an input-feld is set/checked do this
        else {
            $("p.error").hide();
            $(".btn-pick-event").each(function() {
                if ($(this).is(':checked')) {
                    chooseSuggestion = $(this).attr('id');
                    chooseSuggestion = chooseSuggestion.replace("_", " - ");
                    console.log('geschafft')
                }
            });
            
            var event = Events.findOne({_id: this._id});
            //Das Feld fuer den Ersteller entfernen, sonst funktioniert das Speicher nicht
            delete event.creator;
            delete event._id;
            
            event.finalSuggestion = chooseSuggestion;
            event.state = 'closed';
            $.each(event.suggestions, function(i,o){
                if (o.startLocalized + " - " + o.endLocalized === chooseSuggestion) {
                    o.state = suggestionStates.FIXED.value;
                    o.color = suggestionStates.FIXED.color;
                } else {
                    o.state = suggestionStates.DELETED.value;
                    o.color = suggestionStates.DELETED.color;
                }
            });
            
            //var event = { finalSuggestion: chooseSuggestion, state: 'closed',  }
            Meteor.call('updateEvent', currentEventId, event, function(error, response) { 
                if (error) {
                    $("p.error.noUpdate").show(error.response);
                }
                else {
                    sendEventEmail(currentEventId,notificationStates.EVENT_DECIDE);
                    Router.go("eventPage", {_id: currentEventId});
                }
            });
        }
    },

    'click .btn-back': function(e) {
        e.preventDefault();
        var currentEventId = this._id;
        Router.go("eventPage", {_id: currentEventId});
    }
});

Template.eventPicker.rendered = function() { 
    //set Navi-Element to active
    $('.nav-item').removeClass('active');
}
