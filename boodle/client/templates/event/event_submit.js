/** This file is part of Boodle.
  *
  * Boodle is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * Boodle is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with Boodle.  If not, see <http://www.gnu.org/licenses/>.
  */
Template.eventSubmit.events({
    'click #delSuggestions': function(e){    
        deleteCalendarSuggestion(currentSuggestion);
    },
    'click #saveSuggestions': function(e){
        saveCalendarSuggestion(currentSuggestion);
    }
});

Template.eventSubmit.rendered = function () {
  //set Navi-Element to active
  $('.nav-item').removeClass('active');
  $('.nav-item .new-event').parent('.nav-item').addClass('active');
    //Init Taginput with autocomplete
    participantsTagInput(true);
    
    //Collect Current Events and add to Calendar
    fillCalendarWithSuggestions(null, calendarClickEvent);
};

var calendarClickEvent = function(calEvent) { currentSuggestion = calEvent; }

//The Current Suggestion from the Calendar
var currentSuggestion;

// helper functions for eventList
Template.eventSubmit.helpers({
    // get the username to the current session
    getUsername: function()
    {
        var currentUser = Meteor.user().username;
        return currentUser;
    },
    curSuggestion: function()
    {
        return currentSuggestion;
    }    
});

AutoForm.hooks({
  insertEvent: {
        // Called at the beginning and end of submission, respectively.
        // This is the place to disable/enable buttons or the form,
        // show/hide a "Please wait" message, etc. If these hooks are
        // not defined, then by default the submit button is disabled
        // during submission.
        beginSubmit: function(formId, template) {

            $("#eventError").hide();
            
            // create the event from the form 
            var event = createEventObject(true);
            
            if(event.suggestions == null ||event.suggestions == undefined || event.suggestions.length == 0) {
                 $("#eventError").text("Ein Event kann nur mit Terminvorschlägen gespeichert werden.");
                 $("#eventError").show();
            } else {
                Meteor.call('insertEvent', event, function(error, response) {
                    // redirect the user to his shining new event
                    if(response != undefined){
                        sendEventEmail(response,notificationStates.EVENT_INVITATION);
                        Router.go('/event/' + response);
                    }
                }); 
            }
        }
    }
});