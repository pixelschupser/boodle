/** This file is part of Boodle.
  *
  * Boodle is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * Boodle is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with Boodle.  If not, see <http://www.gnu.org/licenses/>.
  */
// TO DO: comments
Template.vote.events({
    'click .voted-not': function(e) {
        e.preventDefault();
        var userId = Meteor.user()._id;
        var eventId = $("#main").attr("data-eventId");
        var date = e.target.name;
        var available = $(e.target).attr("data-value");
        var vote = Votes.findOne({"userId": userId, "eventId": eventId});
        
        //Pruefen ob das Event existiert und ob die Antwort im eingestellten Modus erlaubt ist
        var event = Events.findOne({_id:eventId});
        if(event && (!event.onlyYesNoAnswer || available != "maybe")) {
           
            if(votingAllowed(date,eventId)) {
            
                if (!! vote) {
                    var votes = vote.votes;
                    var contained = false;
                    votes.forEach(function(yetVoted) {
                        if (yetVoted.date === date) {
                            contained = true;
                            yetVoted.available = available;
                        }
                    });
                    if (!contained) {
                        votes.push({
                            date: date,
                            available: available
                        });
                    }
                    Meteor.call('updateVote', vote._id, vote, function(error, response) {

                    });
                } else {
                    vote = {
                        userId: userId,
                        eventId: eventId,
                        votes: [
                            {
                                date: date,
                                available: available
                            }
                        ]
                    };
                    Meteor.call('insertVote', vote, function(error, response) {

                    });
                }
            }
        }
    }
});

// TO DO: comments
Template.vote.rendered = function() {
    var eventId = $("#main").attr("data-eventId");
    var userId = Meteor.user()._id;
    var vote = Votes.findOne({'eventId': eventId, 'userId': userId});
};

// TO DO: comments
Template.vote.helpers({
   hasVoted: function(answer, date, votes, eventId) {
       var result = false;
       
       /*Fallback, beim Speichern eines Votes wird ein automatisches rerendern ausgeloest.
         Bei diesem sind aber die Votes nicht mehr vorhanden, so dass die Anzeige zurueckgesetzt wird.
         D. h. es sieht so aus als wenn man gar nicht gevotet hat. 
         Es war mir nicht moeglich das rerendern zu verhindern. 
        */
       
       if(votes == null || votes == undefined) {
           var userId = Meteor.user()._id;
           var vote = Votes.findOne({"userId": Meteor.user()._id, "eventId": eventId});
           if(vote)
               votes = vote.votes;
       }
       
       if(date != null && date != undefined && votes != null && votes != undefined) {
           var currentDate = getSuggestionIdentifier(date);
           votes.forEach(function(vote) { 
               if(vote.date == currentDate && vote.available == answer)
                   result = true;
           });
       }
      
       return result;
   },
   getIdentifier: function() {
        return getSuggestionIdentifier(this);  
   },
   votingAllowed: function(date, eventId) {
       return votingAllowed(getSuggestionIdentifier(date), eventId);
    },
    getDisplayname: function(username) {
        var user = Members.findOne({username: username});
        return user ? user.displayname : username;
    }
});