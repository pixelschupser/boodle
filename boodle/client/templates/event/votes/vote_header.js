/** This file is part of Boodle.
  *
  * Boodle is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * Boodle is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with Boodle.  If not, see <http://www.gnu.org/licenses/>.
  */
Template.voteHeader.helpers({
    getLocalized: function() {
        var localizedDate = getSuggestionIdentifier(this);
        splitDate = localizedDate.split(' ');
        if (splitDate.length == 2) {
            localizedDate = localizedDate.replace('_', '-');
            return localizedDate;  
        }
        else if (splitDate.length == 3) {
            localizedDate = localizedDate.replace('_', ' ');
            return localizedDate;  
        }
   },
   getFullIdentifier: function() {
        return getFullSuggestionIdentifier(this);  
   }
});
