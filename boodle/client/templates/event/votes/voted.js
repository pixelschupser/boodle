/** This file is part of Boodle.
  *
  * Boodle is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * Boodle is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with Boodle.  If not, see <http://www.gnu.org/licenses/>.
  */

Template.voted.helpers({
   hasVoted: function(date, votes, display, responseMode) {
       /*
        * display=>true: Die tatsaechlich Antowrt soll ausgegeben werden
        *          false: Die css-Klasse zur Antowrt soll ausgegeben werden
        */
       
       var result = "";
       if(date != null && date != undefined && votes != null && votes != undefined) {
           var currentDate = getSuggestionIdentifier(date);
           votes.forEach(function(vote) { 
               if(vote.date == currentDate) {
                   switch(vote.available)
                   {
                        case "yes":
                           result = display ? "Ja" : "voted-yes";
                           break;
                       case "no":
                           //Ist nur eine Antwort je Spalte und Zeile moeglich, soll diese Antwort nicht angezeigt werden
                           if(responseMode == "multi") 
                                result = display ? "Nein" : "voted-no";
                           break;
                       case "maybe":
                           //Ist nur eine Antwort je Spalte und Zeile moeglich, soll diese Antwort nicht angezeigt werden
                           if(responseMode == "multi") 
                                result = display ? "Vielleicht" : "voted-maybe";
                           break;
                   }
               }
           });
       }
       
       return result;
    },
    isOffeneAbstimmung: function(hidden) {
        return !hidden;   
    },
    getDisplayname: function(username) {
        var user = Members.findOne({username: username});
        return user ? user.displayname : username;
    }
});