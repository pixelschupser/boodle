/** This file is part of Boodle.
  *
  * Boodle is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * Boodle is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with Boodle.  If not, see <http://www.gnu.org/licenses/>.
  */
currentEventId = null;

// TO DO: comments
Template.votes.helpers({
    checkVotable: function(user, event) {
        var username = user;
        
        if(username.indexOf("(") > -1) {
            username = username.substring(username.indexOf("(") + 1);
            username = username.substring(0, username.indexOf(")"));
        }
        
        if(username === Meteor.users.findOne({_id:Meteor.userId()}).username) {
            return (!event.deleteEvent) && event.state != "closed";
        }
        
        return false;
    },
    setEventId: function(eventId) {
        currentEventId = eventId;
    },
    getUserVotes: function() {
        //Anhand des Teilnehmernamen dessen Datenbank-Representation ermitteln
        var member = Members.findOne({username:this.toString()});
        if(member != null && member != undefined) {
            //Voting des aktuellen Teilnehmers ermitteln
            var userVoting = Votes.findOne({userId:member._id, eventId: currentEventId});
            
            if(userVoting != null && userVoting != undefined)
                return userVoting;
        }
        return undefined;
    },
    showSummery: function(responseMode) {
        return responseMode == "multi";
    }
});

Template.votes.rendered = function() {
    var currentEvent = Events.findOne({_id: this.data.eventId});
    Meteor.log.debug('current event: ' + currentEvent)
    // get content finalSuggestion
    var eventSelected = currentEvent.finalSuggestion;
    Meteor.log.debug('selected event: ' + eventSelected)
    // get content deleteEvent
    var eventDeleted = currentEvent.deleteEvent;
    Meteor.log.debug('deleted event: ' + eventDeleted)
    // if current page is/has class eventPicker
    var case_a = $('section.event').is('.eventPicker');
    // if the event is chosen/ eventpage has an finalSuggestion
    var case_b = eventSelected != null && eventSelected != undefined && eventSelected != "";
    // if the event is deleted
    var case_c = currentEvent.deleteEvent == "true";
    if( case_a || case_b || case_c ) {
        $('.vote-table').addClass('disabled');
        $('.item-list.event.dropdown').hide();
    }
    if($('#votes').height() >= 400){
        $('#votes').DataTable({
            info: false,
            ordering: false,
            paging: false,
            searching: false,
            scrollX: true,
            scrollY: 400
        });
    }
    else {
        $('#votes').DataTable({
            info: false,
            ordering: false,
            paging: false,
            searching: false,
            scrollX: true
        });
    }
};