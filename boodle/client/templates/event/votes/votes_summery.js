/** This file is part of Boodle.
  *
  * Boodle is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * Boodle is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with Boodle.  If not, see <http://www.gnu.org/licenses/>.
  */


Template.votessummery.helpers({
   sumVotes: function(date, eventId, onlyYesNoAnswer) {
       var result = null;
       if(date != null && date != undefined && eventId != null && eventId != undefined) {
           var currentDate = getSuggestionIdentifier(date);
           var eventVotes = Votes.find({eventId: eventId});
           
           var antwortJa = 0;
           var antwortNein = 0;
           var antwortVielleicht = 0;
           
           if(eventVotes) {
               eventVotes.forEach(function(eventVote) {
                   eventVote.votes.forEach(function(vote) { 
                       if(vote.date == currentDate) {
                           switch(vote.available)
                           {
                                case "yes":
                                   antwortJa++;
                                   break;
                               case "no":
                                   antwortNein++;
                                   break;
                               case "maybe":
                                   antwortVielleicht++;
                                   break;
                           }
               }
                   });
               });
           }
       }
       
       result = $("<div></div>");
       result.append("<p>Ja: " + antwortJa + "</p>");
       result.append("<p>Nein: " + antwortNein + "</p>");
       if(!onlyYesNoAnswer)
            result.append("<p>Vielleicht: " + antwortVielleicht + "</p>");
       
       return result.html();
    }
});