/** This file is part of Boodle.
  *
  * Boodle is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * Boodle is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with Boodle.  If not, see <http://www.gnu.org/licenses/>.
  */
Template.header.events({
    'click .logout': function(e){ 
        e.preventDefault(); 
        var userId = Meteor.userId();
        
        //Benutzer ausloggen (meteor intern)
        Meteor.logout(function(error) {
             if(error)
                console.log(error);
             else {
                //LDAP: unbind durchfuehren  
                Meteor.call("accountLogout", userId, function(error, response) {
                    if(error)
                        console.log(error);
                });
                Router.go('/');
             }
         });
    },
    //go to picker-page of this event to set/choose an option
    'click .new-event': function(e) {
        Router.go("eventSubmit");
    },
    //go to close-page of this event to close the event
    'click .event-overview': function(e) {
        Router.go("eventList");
    },
    'click .administration': function(e) {
        Router.go("administration");
    }
});

Template.header.helpers({
    // get the username to the current session
    isAdmin: function()
    {
        return Meteor.user().admin == true;
    } 
});

