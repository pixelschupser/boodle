/** This file is part of Boodle.
  *
  * Boodle is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * Boodle is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with Boodle.  If not, see <http://www.gnu.org/licenses/>.
  */
Template.startPage.events({
    'click .login-ldap': function(e){  
        e.preventDefault();
        var username = $("#username_ldap").val().toString();
        var password = $("#password_ldap").val().toString();
        
        //Falls die Pruefung der Logindaten laenger dauert, schomal die Fehlermeldung abschalten
        $("p.error.login-ldap").hide();
        
        var loginRequest = { ldapLogin : {username: username, password: password, ldap: true } };
        
        Accounts.callLoginMethod({
            methodArguments: [loginRequest],
            userCallback: function (error, result) {
                if(error) {
                    console.log(error || "Unknown error");
                    $("p.error.login-ldap").show();
                } else {
                    Router.go('/list/');   
                }
            }
        });
    },
    'click .login': function(e){  
        e.preventDefault();
        var username = $("#username").val().toString();
        var password = $("#password").val().toString();
        
        //Falls die Pruefung der Logindaten laenger dauert, schon mal die Fehlermeldung abschalten
        $("p.error.login").hide();
        
        var loginRequest = { ldapLogin : {username: username, password: password, ldap: false } };
        
        Accounts.callLoginMethod({
            methodArguments: [loginRequest],
            userCallback: function (error, result) {
                if(error) {
                    console.log(error || "Unknown error");
                    $("p.error.login").show();
                } else {
                    Router.go('/list/');   
                }
            }
        });
    }
});
