/** This file is part of Boodle.
  *
  * Boodle is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * Boodle is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with Boodle.  If not, see <http://www.gnu.org/licenses/>.
  */

AutoForm.hooks({
  editUserSettings: {
        // Called at the beginning and end of submission, respectively.
        // This is the place to disable/enable buttons or the form,
        // show/hide a "Please wait" message, etc. If these hooks are
        // not defined, then by default the submit button is disabled
        // during submission.
        beginSubmit: function(formId, template) {

            var invitation = $('#invitation-input').is(':checked');
            var changes = $('#changes-input').is(':checked');
            var cancel = $('#cancel-input').is(':checked');
            var decide = $('#decide-input').is(':checked');
            
            var user = Members.findOne({_id: Meteor.user()._id});
            
            if(user) {
                var userId = user._id;
                user.notifications = {
                    emailForEventInvitation: invitation,
                    emailForEventChanges: changes,
                    emailForEventCancel: cancel,
                    emailForEventDecide: decide };
                
                delete user.username;
                delete user.localaccount;
                delete user._id;
                Meteor.call('updateUserSettings', userId, user, function(error, response) { 
                    if(error)
                        alert(error);
                    else
                        Router.go('eventList');
                }); 
            }
        }
    }
});

Template.userProperties.rendered = function() {  
  //set Navi-Element to active
  $('.nav-item').removeClass('active');
  $('.nav-item .properties').parent('.nav-item').addClass('active');
}