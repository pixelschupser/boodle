/** This file is part of Boodle.
  *
  * Boodle is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * Boodle is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with Boodle.  If not, see <http://www.gnu.org/licenses/>.
  */

// creates a collection on server and client side
Events = new Mongo.Collection('events');

EventSchema = new SimpleSchema({
    creator: {
        type: String,
        denyUpdate: true,
        custom: function() {
            return !! Members.findOne({username: this.value});
        }
    },
    title: {
        type: String,
        label: "Titel",
        max: 80
    },
    description: {
        type: String,
        label: "Beschreibung",
        max: 140
    },
    finalSuggestion: {
        type: String,
        optional: true,
        denyInsert: true
    },
    state: {
        type: String,
        optional: true
    },
    participants: {
        type: [String],
        label: "Teilnehmer",
        min: 1
    }, 
    responseMode: {
        type: String,
        allowedValues: ['single', 'multi'],
        label: "Antwortmodus",
        autoform: {
          options: [
            {label: "Mehrere Antworten zulassen", value: "multi"},
            {label: "Nur eine Antwort akzeptieren", value: "single"}
          ]
        }
    }, 
    hidden: {
        type: Boolean,
        label: "Versteckte Umfrage"
    }, 
    onlyYesNoAnswer: {
        type: Boolean,
        label: "Nur Antworten ja und nein erlauben"
    },
    deleteEvent: {
        type: String,
        optional: true
    }, 
    suggestions: {
        type: [Object],
        label: "Terminvorschläge",
        optional: true,
        blackbox: true
    }
});

Events.attachSchema(EventSchema);

