/** This file is part of Boodle.
  *
  * Boodle is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * Boodle is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with Boodle.  If not, see <http://www.gnu.org/licenses/>.
  */
// aliases the users collection, provided by loginButtons
Members = Meteor.users;

NotificationSchema = new SimpleSchema({
   emailForEventInvitation: {
       type: Boolean,
       label: "Benachrichtigung für eine Event-Einladung"
   },
   emailForEventChanges: {
       type: Boolean,
       label: "Benachrichtigung für eine Änderung zum eingeladenen Event"
   },
   emailForEventCancel: {
       type: Boolean,
       label: "Benachrichtigung für die Absages eines Events"
   },
   emailForEventDecide: {
       type: Boolean,
       label: "Benachrichtigung für die Festlegung eines Termins"
   }
});

UserCountrySchema = new SimpleSchema({
    name: {
       type: String
    },
    code: {
        type: String,
        regEx: /^[A-Z]{2}$/
    }
});

UserProfileSchema = new SimpleSchema({
    firstName: {
        type: String,
        regEx: /^[a-zA-Z]{2,25}$/,
        optional: true
    },
    lastName: {
        type: String,
        regEx: /^[a-zA-Z]{2,25}$/,
        optional: true
    },
    birthday: {
        type: Date,
        optional: true
    },
    gender: {
        type: String,
        allowedValues: ['Male', 'Female'],
        optional: true
    },
    website: {
        type: String,
        regEx: SimpleSchema.RegEx.Url,
        optional: true
    },
    bio: {
        type: String,
        optional: true
    },
    country: {
        type: UserCountrySchema,
        optional: true
    }
});

MemberSchema = new SimpleSchema({
    username: {
        type: String,
        label: "Benutzername",
        denyUpdate: true
    },
    displayname: {
        type: String,
        label: "Anzeigename",
        max: 80
    },
    email: {
        type: String,
        label: "E-Mail",
        max: 255
    },
    localaccount: {
        type: Boolean,
        label: "Lokales Konto",
        denyUpdate: true
    },
    admin: {
        type: Boolean,
        label: "Administrator"
    },
    lockedout: {
        type: Boolean,
        label: "Konto gesperrt"
    },
    password: {
        type: String,
        optional: true
    },
    salt: {
        type: String,
        optional: true
    },
    emails: {
        type: [Object],
        optional: true
    },
    "emails.$.address": {
        type: String,
        regEx: SimpleSchema.RegEx.Email
    },
    "emails.$.verified": {
        type: Boolean
    },
    createdAt: {
        type: Date,
        optional: true
    },
    profile: {
        type: UserProfileSchema,
        optional: true
    },
    services: {
        type: Object,
        optional: true,
        blackbox: true
    },
    notifications: {
        type: NotificationSchema,
        optional: true
    }
});

Members.attachSchema(MemberSchema);