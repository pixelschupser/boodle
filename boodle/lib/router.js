/** This file is part of Boodle.
  *
  * Boodle is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * Boodle is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with Boodle.  If not, see <http://www.gnu.org/licenses/>.
  */
// configuration of predefined router hooks
Router.configure({
    layoutTemplate: 'layout',
    waitOn: function() { return Meteor.subscribe('events'); },
    notFoundTemplate:'404'    
});

// route to the start page
Router.route('/', {name: 'startPage'});
// route to close a submited page
Router.route('/close/:_id', {
    name: 'eventClose',
    data: function() { return Events.findOne(this.params._id); }
});
Router.route('/closed/:_id', {
    name: 'eventClosedPage',
    data: function() { return Events.findOne(this.params._id); }
});
// route to the edit submited page
Router.route('/edit/:_id', {
    name: 'eventEdit',
    data: function() { return Events.findOne(this.params._id); }
});
// route to the overview page
Router.route('/list/', {
    name: 'eventList'
});
// route to the set a decision to a submited page
Router.route('/pick/:_id', {
    name: 'eventPicker',
    data: function() { return Events.findOne(this.params._id); }
});
// route to the submit page
Router.route('/submit/', {
    name: 'eventSubmit'
});
// dynamical route to the detailed view of an event
Router.route('/event/:_id', {
    name: 'eventPage',
    data: function() { return Events.findOne(this.params._id); }
});

// dynamical route to the detailed view of an User
Router.route('/user/properties', {
    name: 'userProperties',
    data: function() { return Meteor.user(); }
});

// ICS Export Links
Router.route('/ICS/Export/Fixed/:_id', function () {
    this.response.end(exportEvents_Fixed().download('Boodle_Events_Fixed'));
});
Router.route('/ICS/Export/Open/:_id', function () {
    this.response.end(exportEvents_Open().download('Boodle_Events_Open'));
});
Router.route('/ICS/Export/All/:_id', function () {
    this.response.end(exportEvents_All().download('Boodle_Events_All'));
});

Router.route('/administration/', {
    name: 'administration' 
});

Router.route('/administration/account/:_id', {
    name: 'account',
    data: function() { return Members.findOne(this.params._id); }
});
Router.route('/administration/account/', {
    name: 'newaccount',
    data: function() { return { localaccount: true }; }
});

// guard to deny access for not logged in users
function requireLogin() {
    if (! Meteor.user()) {
        this.render('accessDenied');
    } else {
        this.next();
    }
}

function requireAdminright() {
    if (Meteor.user() && Meteor.user().admin) {
        this.next();
    } else {
        this.render('accessDenied');  
    }
}

// guard hook for submision of a new event
Router.onBeforeAction(requireLogin, {except: 'startPage'});

Router.onBeforeAction(requireAdminright, {only: ['account', 'newaccount', 'administration']});

// Beispielseite für Iconfont
Router.route('/icon-font', {name: 'iconfont'});
