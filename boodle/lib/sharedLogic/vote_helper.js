/** This file is part of Boodle.
  *
  * Boodle is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * Boodle is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with Boodle.  If not, see <http://www.gnu.org/licenses/>.
  */

votingAllowed = function(date, eventId) {
    var alreadyVoted = false;
    
    if(date != null && date != undefined && eventId != null && eventId != undefined) {
       var userId = Meteor.user()._id;
       var currentDate = date;
       var eventVotes = Votes.find({eventId: eventId});
       var event = Events.findOne({_id: eventId});    
       
       //Pruefung muss nur fuer den Antwortmodus "Nur eine Antwort je Zeile und Spalte" erfolgen
       if(eventVotes && event && event.responseMode == "single") {
           eventVotes.forEach(function(eventVote) {
               if(eventVote.userId == userId) {
                   //eigene Votes: pruefen ob fuer irgendeinen anderen Terminvorschlag ein "yes" Vote exisitiert
                   eventVote.votes.forEach(function(vote) { 
                       if(vote.date != currentDate && vote.available == "yes") {
                           alreadyVoted = true;
                           return;
                       }
                   });
               } else {
                   //fremde Votes: pruefen ob fuer diesen Terminvorschlag ein "yes" Vote exisitiert
                   eventVote.votes.forEach(function(vote) { 
                       if(vote.date == currentDate && vote.available == "yes") {
                           alreadyVoted = true;
                           return;
                       }
                   });
               }

               if(alreadyVoted)
                   return;
           });
       }
   }

   return !alreadyVoted;
};