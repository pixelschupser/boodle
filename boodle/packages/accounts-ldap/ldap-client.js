var Future  = Npm.require('fibers/future');
var ldap    = Npm.require('ldapjs');

var userLDAPBinds = [];

ldap.Attribute.settings.guid_format = ldap.GUID_FORMAT_B;

LDAP = {};

LDAP.ldap = ldap;

// Settings
LDAP.serverIP = 'rsw011.beuth-hochschule.de';
LDAP.serverPort = 636;
LDAP.searchOu = 'OU=Accounts,DC=beuth-hochschule,DC=de';

LDAP.performUnbind = function(username) {
    var userLDAPBindsValid = [];
    
    for(index = 0; index < userLDAPBinds.length; index++) {
        if(userLDAPBinds[index].username === username) {
            userLDAPBinds[index].bind.unbind(function(err) { /* Ergebnis interessiert nicht */});
            //Auch wenn eine Uebereinstimmung gefunden wurde weiter suchen, 
            //falls etvl. mal ein bind nicht entfernt wurde
        }
        else{
            userLDAPBindsValid.push(userLDAPBinds[index]);
        }
    }
    
    userLDAPBinds = userLDAPBindsValid;
}

LDAP.performSearch = function(currentUsername, searchname) {
   var future = new Future;
    var dn = [];
    var userBind = null;
    var searchQuery = {
        filter: "(anr=" + searchname + ")",
        scope: 'sub',
        attributes: ["displayName", "mail", "givenName", "name", "sn", "cn"]
    };
    
    //Bind des aktuellen Users ermitteln
    for(index = 0; index < userLDAPBinds.length; index++) {
        if(userLDAPBinds[index].username == currentUsername) {
            userBind = userLDAPBinds[index].bind;
            break;
        }
    };
    
    if(userBind == null)
        return null;
    
    userBind.search(LDAP.searchOu, searchQuery, function (err, search) {

        if(err)
            console.log(err);
        
        search.on('searchEntry', function (entry) {
            dn.push(entry.object);
        });

        search.on('error', function (err) {
            future['return'](null)
        });

        search.on('end', function () {
            if (dn.length === 0) {
                future['return'](null);
            }else{
                future['return'](dn);
            }
        });

    });
    
    return future.wait();
};

LDAP.checkAccount = function(options) {
  options = options || {};

  if (options.hasOwnProperty('username') && options.hasOwnProperty('password')) {

    var future = new Future;
    var dn = [];

    var userBind = LDAP.ldap.createClient({
      url: 'ldaps://' + LDAP.serverIP + ':' + LDAP.serverPort,
      timeout:  3000 /* Falls LDAP nicht erreichbar ist soll nach 3 Sekunden abgebrochen werden */   
    }); 
    
    userBind.bind(options.username, options.password, function (err) {
        if(err) {
            if(err.toString().indexOf("timeout") > -1)
                future['return'](LDAPBINDRESULT.TIMEOUT);
            else
                future['return'](LDAPBINDRESULT.ERROR);
        }else {
            future['return'](LDAPBINDRESULT.SUCCESS);
        }
    });  
      
    var bindSuccess = future.wait();

    if(bindSuccess) {
        userLDAPBinds.push({ username: options.username, bind: userBind});
    }
      
    return bindSuccess;  
      
  } else {
    throw new Meteor.Error(400, "Missing Parameter");
  }
};

LDAPBINDRESULT = {
    SUCCESS: { value: 1 },
    ERROR: { value: 0 },
    TIMEOUT: { value: 2 }
};