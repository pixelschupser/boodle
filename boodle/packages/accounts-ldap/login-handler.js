Meteor.startup(function() {
   process.env.RUNTIME_ENV = "develop"; 
});

Accounts.registerLoginHandler("ldap", function(loginRequestNew) {
    var loginRequest = loginRequestNew.ldapLogin;
    var develop = process.env.RUNTIME_ENV == "develop";    
    var email = loginRequest.username;
    
    if((develop || loginRequest.ldap) && email.indexOf('@beuth-hochschule.de') == -1)
        email += '@beuth-hochschule.de';
    
    //Zuerst pruefen, ob der Authentifizierungsprozess umgangen werden soll
    if(develop && process.env.BOODLE_ADMIN != loginRequest.username) 
        return loginAsDevelop(email, loginRequest.username, loginRequest.password);
    
    //Der Admin-Account soll immer lokal angemeldet werden
    if(loginRequest.ldap && process.env.BOODLE_ADMIN != loginRequest.username)
    {
        //Authentifizierung ueber ldap (Fallback ueber die lokalen Accounts)  
        return loginWithLDAP(email, loginRequest.username, loginRequest.password);
    }
    else
    {
        //Es koennen nur als extern markierte Benutzer angemeldet werden 
        return loginLocal(loginRequest.username, loginRequest.password, true);
    }
    
    return undefined; /* Hier muss unbedingt undefined zurueckgegeben werden. */
});

var loginWithLDAP = function(email, username, password) {
    //Login erfolgt ueber die Authentifizierung von LDAP
    //Ist LDAP nicht verfuegbar wird versucht die Anmeldung ueber die lokalen Accounts durchzufuehren
    var ldapResult = LDAP.checkAccount({ username: email, password: password });
    
    if(ldapResult.value == LDAPBINDRESULT.SUCCESS.value) {
        //Anmeldung ueber LDAP erfolgreich
        
        //Zur Sicherheit noch mal pruefen ob der Benutzername auch wirklich der Benutzername ist und nicht die gesamte E-Mail Adresse
        if(username.indexOf('@beuth-hochschule.de') > -1)
            username = username.substring(0, username.indexOf("@"))
        
        var userId;
        var user = Meteor.users.findOne({ username : username });
        
        if (user) {
            if(user.lockedout) {
                //Wurde das Benutzerkonto gesperrt, 
                //wird der bind wieder beendet und der Login schlaegt fehl
                accountLogout(user._id);
                return undefined;
            }else{
                userId = user._id;
                addNotificationObject(user); //Entfernt u. u. das ID Attribute
                updateUserPassword(user, password); //Entfernt u. u. das ID Attribute
            }
        }
        else
        {
            userId = insertUser(email, email, true, password);
        }
            
        return { userId: userId };
    }
    else if(ldapResult.value == LDAPBINDRESULT.TIMEOUT.value)
    {
        //Kein LDAP verfuegbar, also versuchen sich mit einem lokalen Account anzumelden
        return loginLocal(username, password, false);
    }
    
    return undefined; /* Hier muss unbedingt undefined zurueckgegeben werden. */
};

var loginLocal = function(username, password, localaccount) {
    var user = Meteor.users.findOne({ username : username });
    
    if(user && user.localaccount == localaccount && !user.lockedout && checkPassword(user, password)) {
        var userId = user._id;
        addNotificationObject(user); //Entfernt u. u. das ID Attribute
        return { userId: userId };
    }
    
    return undefined; /* Hier muss unbedingt undefined zurueckgegeben werden. */
};

var loginAsDevelop = function(email, displayname, password) {
    Meteor.log.debug("loginmode: develop");
    //Login erfolgt ohne die Authentifizierung durch LDAP
    //Ist bei der Anmeldung der Account nicht vorhanden, wird dieser automatisch angelegt
    var username = email.substring(0, email.indexOf("@"));
    var user = Meteor.users.findOne({ username : username });
    var userId;
        
    if (user) {
        userId = user._id;
        addNotificationObject(user); //Entfernt u. u. das ID Attribute
    } else {
        userId = insertUserInDBGeneratePassword(username, email, displayname, password, false);
    }
    
    return { userId: userId };
};