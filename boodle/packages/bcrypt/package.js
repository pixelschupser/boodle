Package.describe({
  summary: "bcrypt for meteor"
});

Npm.depends({'bcrypt' : '0.8.0'});

Package.on_use(function (api) {
  api.add_files('bcrypt.js', 'server');
  api.export('BCrypt', 'server');
});

