/** This file is part of Boodle.
  *
  * Boodle is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * Boodle is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with Boodle.  If not, see <http://www.gnu.org/licenses/>.
  */
Meteor.startup(function() {
    process.env.BOODLE_ADMIN = "boodleAdmin"; 
    var user =  Members.findOne({username: process.env.BOODLE_ADMIN });
    
    if(!user) {
        var userId = insertUserInDBGeneratePassword(process.env.BOODLE_ADMIN, process.env.BOODLE_ADMIN ,"admin","admiN@boodl3",true);
        user =  Members.findOne({_id: userId });
        user.admin = true;
        delete user.username;
        delete user.localaccount;
        delete user._id;
        Meteor.users.update(userId, { $set: user });
    }
});

Meteor.methods({
    accountLogout: function(userId) {
        var user =  Members.findOne({_id: userId});
        
        if(user != null)
          LDAP.performUnbind(user.username + "@beuth-hochschule.de");
        
        return false;
    },
    ldapSearch: function(currentUsername, searchUsername) {
        return LDAP.performSearch(currentUsername + "@beuth-hochschule.de", searchUsername);
    },

    updateUseraccount: function(userId, useraccount) {
        if(Meteor.user() && Meteor.user().admin) {
            
            if(userId) {
                //das Passwort das in dieser Methode nicht veraendert werden
                delete useraccount.password;
                delete useraccount.salt;
                Meteor.users.update(userId, { $set: useraccount });
            } else {
                var userDB = Members.findOne({username: useraccount.username});
                
                if(userDB) {
                    throw new Meteor.Error(400, "Ein Benutzer mit dem angegebenen Benutzernamen existiert bereits.");   
                } else {
                var password = randomString(10);
                userId = Meteor.users.insert(useraccount);
                updateUserPassword(Members.findOne({_id: userId}), password);
                sendEmailNewAccount(useraccount.email, useraccount.username, useraccount.displayname, password, false);
                }
            }
        } else {
            throw new Meteor.Error(400, "Unauthorized access to user");   
        }
    },
    updateUserSettings: function(userId, user) {
        if(Meteor.user() && Meteor.user()._id == userId) {
            
                //das Passwort das in dieser Methode nicht veraendert werden
                delete user.password;
                delete user.salt;
                Meteor.users.update(userId, { $set: { notifications: user.notifications } });
            
        } else {
            throw new Meteor.Error(400, "Unauthorized access to user");   
        }
    },
    resetpassword: function(userId) {
        if(Meteor.user() && Meteor.user().admin) {
            var user = Members.findOne({_id: userId});
            if(user.localaccount) {
                var password = randomString(10);
                updateUserPassword(user, password);
                sendEmailNewAccount(user.email, user.username, user.displayname, password, true);
            }
        }
        else {

        }
    }
});

function Exception(name, message) {
    return {
        name: name,
        message: message,
        toString: function() { return this.name + ": " + this.message }
    }
}

/**
  * authorize a user to perform state changes of other users by checking if she owns admin-state
  */
authorizeAdmin = function() {
    if ( ! Meteor.user().is_admin || Meteor.user().is_blocked ) {
        throw new Exception(
            "AuthenticationException", 
            "unauthorized change of user-state - you have to own admins state to change user state."
        );
    }
}
var sendEmailNewAccount = function(email, username, displayname, password, reset) {
    var message = 'Hallo ' + displayname;
    
    if(reset)
        message+=',\n\nIhr Passwort wurde zurückgesetzt.\n\n';
    else
        message+=',\n\nfür Sie wurde ein Boodle-Konto erstellt.\n\n';
    
    message+= "\n\n";
    message+="https://boodle.beuth-hochschule.de";
    message+= "\n\n";
    
    message+='Benutzername: ' + username + '\n';
    message+='Passwort: ' + password;
    
    Meteor.call('sendEmail',
            email,
            'no-reply-boodle@beuth-hochschule.de',
            'Boodle: Benutzerkonto',
            message);
}


var randomString = function(length) {
  
    var retVal = "";

    for(var i = 0; i < length;i++) {
        var r = Math.random() * 52 << 0;
        retVal += String.fromCharCode(r+= r > 9 ? (r < 36 ? 55: 61) : 48);
    }
    
    return retVal;
};

checkParticipants = function(participants) {
    
    if(participants != null && participants != undefined && participants.length > 0) {
        var user =  Members.findOne({_id: Meteor.user()._id});
       
        for(index = 0; index < participants.length; index++) {
            var ldapusernameSearch = "";    
            
            //Der Text in Klammern sollte der Username des Teilnehmers sein
            if(participants[index].indexOf("(") > -1) {
                ldapusernameSearch = participants[index].substring(participants[index].indexOf("(") +1);   
                ldapusernameSearch = ldapusernameSearch.substring(0, ldapusernameSearch.indexOf(")"));
            } else {
                ldapusernameSearch = participants[index];
            }
            //Zur Sicherheit alle Leerzeichen entfernen
            ldapusernameSearch = ldapusernameSearch.trim();
            
            //Prüfen, ob der Benutzer bereits in der DB enthalten ist und wenn nicht einfügen
            if(Members.findOne({username:ldapusernameSearch}) == null) {
                ldapusernameSearch += "@beuth-hochschule.de";
                var participantId = insertUser(user.username + "@beuth-hochschule.de", ldapusernameSearch, false, null);
                /*if(participantId != undefined)
                {
                   var participant = Members.findOne({_id: participantId})
                   participants[index] = participant.displayname + " (" + participant.username + ")";
                }*/
            }
            
        }
    }
}

insertUser = function(ldapusername, username, withPassword, password) {
    var search = LDAP.performSearch(ldapusername, username);
    
    /* Da Dozenten keine eindeutige S-Nummer haben sondern ihren Nachnamen als CN,
     * wird als Username der der Teil vor dem @ der E-Mail Adresse verwendet.
     * Bei Studenten ist das die S-Nummer.
     */
                    
    if(search != null && search != undefined && search.length == 1) {
        var salt = null; //withPassword ? bcrypt.genSaltSync(10) : null;
        var hash = null; //withPassword ? bcrypt.hashSync(password, salt) : null;
        
        return insertUserInDB(search[0].mail.substring(0,search[0].mail.indexOf("@")), 
                              search[0].mail,
                              search[0].displayName,
                              hash,
                              salt,
                              false);    
    }
    
    return undefined;
}

insertUserInDB = function(username, email, displayname, passwordHashed, salt, localaccount) {
     return Meteor.users.insert({ username: username, 

                                  email: email,
                                  displayname: displayname,
                                  password: passwordHashed,
                                  salt: salt,
                                  admin: false,
                                  localaccount: localaccount,
                                  lockedout: false,
                                  notifications: {
                                        emailForEventInvitation: true,
                                        emailForEventChanges: true,
                                        emailForEventCancel: true,
                                        emailForEventDecide: true }
                                    });   
}

insertUserInDBGeneratePassword = function(username, email, displayname, password, localaccount) {
    var salt = BCrypt.genSaltSync(10);
    var hash = BCrypt.hashSync(password, salt);
    return insertUserInDB(username,email,displayname,hash,salt, localaccount);  
}

updateUserPassword = function(user, password) {
    
    if(user.salt == null || user.salt == undefined || !checkPassword(user, password))
    {
        //Im Account ist kein Password eingetragen oder das Passwort hat sich geanedert
        var userId = user._id;
        user.salt = BCrypt.genSaltSync(10);
        user.password = BCrypt.hashSync(password, user.salt);
        delete user.username;
        delete user.localaccount;
        delete user._id;
        Meteor.users.update(userId, { $set:  user });   
    }
}

checkPassword = function(user, password) {
    return (user &&  
            user.salt != null &&  
            user.salt != undefined && 
            BCrypt.hashSync(password, user.salt) == user.password);
}

addNotificationObject = function(user) {
    if(user && (!user.notifications || user.notifications == null || user.notifications == undefined)) {
        var userId = user._id;
        user.notifications = {
            emailForEventInvitation: true,
            emailForEventChanges: true,
            emailForEventCancel: true,
            emailForEventDecide: true };
        
        delete user.username;
        delete user.localaccount;
        delete user._id;
        Meteor.users.update(userId, { $set:  user });  
    }
}