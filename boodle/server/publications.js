/** This file is part of Boodle.
  *
  * Boodle is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * Boodle is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with Boodle.  If not, see <http://www.gnu.org/licenses/>.
  */
/**
  * The following methods publish changes on serverside 
  * to the collections to the clients.
  */
Meteor.publish('events', function() {
    return Events.find();
});

Meteor.publish('votes', function() {
    return Votes.find();
});

Meteor.publish('members', function() {
    // publishes only selected fields 
    return Members.find({}, {fields: {_id: true, 
                                      username: true, 
                                      email:true, 
                                      displayname:true, 
                                      localaccount: true, 
                                      admin: true,
                                      lockedout: true,
                                      notifications: true } }); 
});

function Exception(name, message) {
    return {
        name: name,
        message: message,
        toString: function() { return this.name + ": " + this.message }
    }
}

/**
  * The following functions authorize for changes to the collections.
  *
  * If the current user is not allowed to perform the modification,
  * it throws an exception.
  */
function authorizeEventInsert(creator) {
    if (creator != Meteor.user().username) {
        throw Exception(
            "Unauthorized access to event", 
            "You have to be logged in to create an event."
        );
    }
}

function authorizeEventChangeFor(eventId) {
    var eventOwner = Events.findOne({'_id': eventId}).creator;
    if (eventOwner != Meteor.user().username){
        throw Exception(
            "Unauthorized access to event",
            "Only the creator is allowed to change an event."
        );
    }
};

function authorizeVoteInsert(voter) {
    if (voter != Meteor.user()._id) {
        throw Exception(
            "Unauthorized access to vote",
            "You have to be logged in to vote."
        );
    }
};

function authorizeVoteChangeFor(voteId) {
    var voteOwner = Votes.findOne({'_id': voteId}).userId;
    if (voteOwner != Meteor.user()._id) {
        throw Exception(
            "Unauthorized access to vote",
            "A user can only vote for himself."
        );
    }
}

function checkSuggestions(suggestions) {
    if(suggestions == null || suggestions == undefined || suggestions.length == 0) {
        throw Exception(
            "Ungültige Eventdaten",
            "Ein Event muss mindestens einen Terminvorschlag haben"
        );
    }
}

function pruefeErlaubteAntwortmodus(vote)
{
    var event = Events.findOne({_id:vote.eventId});
    if(event) {
       vote.votes.forEach(function(singleVote) {
           
           if(singleVote.available != "yes" || votingAllowed(singleVote.date, vote.eventId)) {
           
           if(event.onlyYesNoAnswer && (singleVote.available != "yes" || singleVote.available != "no"))
              return false;
              
            if(!event.onlyYesNoAnswer && (singleVote.available != "yes" || singleVote.available != "no" || singleVote.available != "maybe"))
               return false;
           }
           
           return false;
       });
               
       return true;
    }
    
    return false;
}

var checkEventSettings = function(event) {
    if(event.responseMode == "single") {
        //Die Antwort vielleicht hat keinen Sinn bei dieser Einstellung
        event.onlyYesNoAnswer = true; 
        //Eine versteckte Umfrage macht keinen Sinn, wenn nur eine Antwort je Zeile und Spalte erlaubt ist
        event.hidden = false;
    }
}

Meteor.methods({
    insertEvent: function(doc) {
        try {
            authorizeEventInsert(doc.creator);
            checkSuggestions(doc.suggestions);
        } catch (e) {
            Meteor.log.error(e.toString());
           throw new Meteor.Error(500, e.toString);
        }
         checkEventSettings(doc);
        checkParticipants(doc.participants);
        
        return Events.insert(doc);
    },
    updateEvent : function (eventId, eventObject) {
        try {
            authorizeEventChangeFor(eventId);
            checkSuggestions(eventObject.suggestions);
        } catch (e) {
            Meteor.log.error(e.toString());
            throw new Meteor.Error(500, e.toString);
        }
        checkParticipants(eventObject.participants);
        checkEventSettings(eventObject);
        Events.update(eventId, { $set: eventObject });
    },
    insertVote: function(vote) {
        try {
            authorizeVoteInsert(vote.userId);
        } catch (e) {
            Meteor.log.error(e.toString());
            throw new Meteor.Error(500, e.toString);
        }
        
        if(pruefeErlaubteAntwortmodus(vote))
            Votes.insert(vote);
    },
    updateVote: function(voteId, vote) {
        try {
            authorizeVoteChangeFor(voteId);
        } catch (e) {
            Meteor.log.error(e.toString());
            throw new Meteor.Error(500, e.toString);
        }
        
        if(pruefeErlaubteAntwortmodus(vote))
            Votes.update(voteId, { $set:  {votes: vote.votes} });
    }
});

Meteor.log.debug('hello boodle');