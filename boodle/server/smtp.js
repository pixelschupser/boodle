/** This file is part of Boodle.
  *
  * Boodle is free software: you can redistribute it and/or modify
  * it under the terms of the GNU General Public License as published by
  * the Free Software Foundation, either version 3 of the License, or
  * (at your option) any later version.

  * Boodle is distributed in the hope that it will be useful,
  * but WITHOUT ANY WARRANTY; without even the implied warranty of
  * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  * GNU General Public License for more details.

  * You should have received a copy of the GNU General Public License
  * along with Boodle.  If not, see <http://www.gnu.org/licenses/>.
  */
// URL of the SMTP server
// smtp://USERNAME:PASSWORD@HOST:PORT/
process.env.MAIL_URL="smtp://relay.beuth-hochschule.de";

Meteor.methods({
   /* send an email using meteor email package
   usage:
   In your client code: asynchronously send an email
      Meteor.call('sendEmail',
            's55817@beuth-hochschule.de,s52388@beuth-hochschule.de,s55648@beuth-hochschule.de,s53925@beuth-hochschule.de,ghervin.diduch@gmail.com',
            'boodle.mailbot@beuth-hochschule.de',
            'HELLO FROM BOODLE-SERVER',
            'It is not easy to send emails when the dns is not working properly!');
  */
  sendEmail: function (to, from, subject, text) {
    check([to, from, subject, text], [String]);

    // Let other method calls from the same client start running,
    // without waiting for the email sending to complete.
    this.unblock();

    Email.send({
      to: to,
      from: from,
      subject: subject,
      text: text
    });
  }
});


